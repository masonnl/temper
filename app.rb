require "sinatra"
require "lazy_high_charts"

# Require helpers
require_relative "helpers/temperature"

# Begin class Temper
class Temper < Sinatra::Base

  # Set ENV files for reading later
  ENV["base"]     = File.dirname(__FILE__) # Temper root
  ENV["config"]   = File.join(ENV["base"], "config.json")
  ENV["logs"]     = File.join(ENV["base"], "logs")
  ENV["errors"]   = File.join(ENV["logs"], "errors.log")
  ENV["readings"] = File.join(ENV["logs"], "readings.json")
  ENV["hours"]    = "12"

  helpers TempHelper
  include LazyHighCharts::LayoutHelper

  # Hello World
  get '/' do
    duration = (params[:duration] || ENV["hours"]).to_i*60*60 #=> to seconds
    @monitor = TempMonitor.new(duration)
    build_highchart(@monitor.readings)
    @duration_options = [1, 2, 4, 8, 16, 32, 40] # Array(1..15)
    @current_day = @duration_options.delete(duration/60/60/24) || @duration_options.delete(1) #=> to days

    haml :index, layout: :layout
  end

  private

  def build_highchart(readings)
    datetimes   = readings.flat_map(&:keys) || []
    clean_hours = datetimes.map{ |date| date.strftime("%h-%d @ %H:%M") }
    temps       = readings.flat_map(&:values).map{ |h| h["fahrenheit"].to_f }

    @chart = LazyHighCharts::HighChart.new('graph') do |f|
      f.title({text: "Temperature Over Time"})
      f.options[:chart][:type] = "area"
      f.options[:chart][:zoomType] = "x"
      f.options[:xAxis][:title] = "Date/Time"
      f.options[:xAxis][:tickInterval] = ENV["hours"].to_i # x-axis label interval
      f.options[:xAxis][:categories] = clean_hours
      f.options[:yAxis][:title] = "Temperature"
      f.options[:yAxis][:min] = 65
      f.series(type: "area", name: "Temperature", data: temps)
    end
  end

end # End class Temper

