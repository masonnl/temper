# Temper
A Tool for monitoring the cluster room

# Configuration
### Setting email notifications:
Controlled by `email.json` file, which is _not_ under git management.
Format of file (if needing to create), is: `{"threshold":70,"last_email":"20yy-mm-dd hh:mm:ss","from":"temper@host.name","to":["email1","email2","etc"],"subject": temper stats","body":"See <a href='appropriate-URL'>link body text</a> for more details."}`,
where:

- the threshold number is the temperature at which an email is sent,
- the date and time is for the last alert sent 
(which is somewhat moot if configuring for the first time but don't put it in the future),
- the host.name is the machine running the monitor,
- the list of emails are the intended recipients of the alert,
- the appropriate-URL is link to the running app
- and the link body text is whatever you want
(and of course the message fields can be changed to suit your local needs)

### To start the unicorn server:
`cd /srv/www/temper`
`unicorn -c config/unicorn.rb -D`

* Note that if anything changes in `app.rb`, a unicorn server restart is required.

# Known Bugs
* Sometimes unicorn won't start due to permissions on `/tmp`. I've [Mason] have been running `chown -R temper /tmp` to temporarily fix issues... There is bound to be a better solution.

# To Do's
- add stop and restart script to stop and restart the unicorn server
- have different severity levels for different temperature thresholds with different alert frequencies
- at the highest severity/threshold (say, 80 degrees), 
run a script on the cluster that will shutdown all the compute nods
- find and use a (free) text-message service to send texts at certain threshold(s)
- at some frequency (say, 5 days), email a graphic of the temperature regardless of any threshold events
