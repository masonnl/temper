require 'json'
require 'mail'
require 'time'

task :default do

  # Query the temperature sensor for a current temperature reading
  #=> "2015/10/15 20:21:20 Temperature 65.75F 18.75C"
  reading = `/home/temper/usb-thermometer/pcsensor`.split(/\s+/)

  # Be verbose about variables for maintainability
  date       = reading.first
  time       = reading[1]
  fahrenheit = reading[3].chomp("F")
  celcius    = reading.last.chomp("C")

  #=> Hash
  json = {date => nil}
  json[date] = {time => nil}
  json[date][time] = { fahrenheit: fahrenheit }
  json[date][time].update Hash[celcius: celcius]

  ## Parse the reading into comprehensive JSON for data retrieval
  # Ensure a readings file exists
  readings_path = "logs/readings.json"
  if !File.exist?(readings_path)
    FileUtils.mkdir_p("logs") && File.open(readings_path, "w")
  end

  # Read in existing JSON
  readings = File.open(readings_path, "r") do |file|
    File.zero?(file) ? {} : JSON.parse(file.read)
  end

  # Insert new data
  if readings.has_key?(date)
    readings[date] = readings[date].merge!(json[date])
  else
    readings.update(json)
  end

  # JSONify the new hash and close the file
  File.open(readings_path, "w") { |f| f.write JSON.pretty_generate(readings, {indent: "\t", object_nl: "\n"}) }

  # Invoke a warning check with the current reading
  Rake::Task[:check].invoke(fahrenheit)
end

# Check that the reading does not exceed the threshold
# If it does, send emails
task :check, :reading do |t, args|
  reading = args[:reading]
  email  = JSON.parse(File.read("email.json"))
  threshold    = email["threshold"]
  last_emailed = Time.parse(email["last_email"])
  current_time = Time.new

  # If reading exceeds the threshold && no emails were sent recently, send an email
  if reading.to_i >= threshold && last_emailed.to_i+3600 < current_time.to_i
    Mail.defaults do
      delivery_method :smtp, { openssl_verify_mode: "none" }
    end

    Mail.deliver do
      from     email["from"]
      to       email["to"]
      subject  email["subject"]
 
      html_part do
        content_type 'text/html; charset=UTF-8'
        body "<p style='font-size:15px;'>#{reading}°F on #{current_time.strftime("%B %d, %Y @ %H:%M:%S")}</p><p>#{email["body"]}</p>"
      end
    end

    # Record the current time and rewrite JSON
    email["last_email"] = current_time.strftime("%Y-%m-%d %H:%M:%S")
    File.open("email.json", "w") { |f| f.write(JSON.generate(email)) }
  end
end

