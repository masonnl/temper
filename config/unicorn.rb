@dir = "/srv/www/temper"

# Set the working application directory
working_directory @dir

# Unicorn PID file location
pid "#{@dir}/pids/unicorn.pid"

# Path to logs
stderr_path "#{@dir}/logs/unicorn.log"
stdout_path "#{@dir}/logs/unicorn.log"

# Unicorn socket
listen "/tmp/unicorn.temper.sock"

# Number of processes
worker_processes 1

# Time-out
timeout 30
