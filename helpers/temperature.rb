require 'date'
require 'json'

module TempHelper
  class TempMonitor

    attr_reader :readings, :current
    def initialize(duration)
      @current  = sanitize_reading
      @readings = get_readings(duration)
    end

    private

    # Read in the readings.json file
    #=> Hash
    def get_readings(duration)
      readings = JSON.parse(File.read(ENV["readings"]))
      arr, now = [], Time.now
      # TODO: Would be a nice place to implement a quicksort instead of looping over all the values
      readings.each do |day,reading|
        reading.each do |time,data|
          reading_time = Time.parse("#{day} #{time}")
	  if reading_time > (now - duration)
            arr << Hash[reading_time, data]
          end
        end
      end
      arr
    rescue StandardError => e
      log(e)
    end

    def sanitize_reading
      `/home/temper/usb-thermometer/pcsensor`.split(/\s+/)[3].chomp("F")
    end

    # Log a StandardError to the error.log
    #=> nil
    def log(error)
      time = Time.now.strftime("%Y-%m-%d %H:%M:%S")
      FileUtils.mkdir_p(ENV["logs"]) unless File.file?(ENV["errors"])
      File.open(ENV["errors"], "a+") { |f| f.write(time + error.inspect + "\n") }
    end

  end # end class Temp
end # end module TempHelper

